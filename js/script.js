const student = {
    name: undefined,
    lastName: undefined,
    askInitials() { 
        this.name = prompt("Enter the name");
        this.lastName = prompt("Enter the last name") 
    },
    createTabel() {
        this.tabel = {};
        do {
            subject = prompt("Enter the name of the subject");
            if (!subject) { break;}
            this.tabel[subject] = +prompt("Enter a grade of " + subject + " subject");
        } while (subject);
    },
    calculateLowGrades() {
        let lowGrades = 0;
        for (const subject in this.tabel) {
            if (4 > this.tabel[subject]) {
                lowGrades++;
            }
        };
        if(lowGrades === 0) { alert("Студента переведено на наступний курс") };
    },
    calculateAverageGrade() {
        let gradesSum = 0;
        for (const subject in this.tabel) {
            gradesSum += this.tabel[subject];
        };
        const averageGrade = gradesSum / Object.keys(this.tabel).length;
        if (7 < averageGrade) { alert("Студенту призначено стипендію") };
    }
};

student.askInitials();
student.createTabel();
student.calculateLowGrades();
student.calculateAverageGrade();